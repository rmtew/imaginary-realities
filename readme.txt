SETUP

1. Install Python 2.7.9 if not already installed.
1. `c:\python27\pip install virtualenv`
2. `c:\python27\scripts\virtualenv _env`
3. `_env\Scripts\activate`
4. `pip install jinja2`
5. `pip install feedformatter`

SETUP - EBOOK GENERATION

1. Download calibre and install
	a) For linux shared hosting (from http://calibre-ebook.com/download_linux):
		1) Remove sudo commands from binary fetcher/installer command line.
		2) Download and edit the linked 'linux-installer.py', replace '/opt' with '~/bin' or whatever.
		3) 'cat linux-installer.py |' to the python command in the fetcher/installer command line.
		4) Ignore installation errors.
		5) May get errors generating pdf, if so, do offline.
	b) For windows, install calibre 2+.
2. The generation script will warn you if it cannot find calibre, and cannot generate the e-books.

OPTIONAL SETUP

1. `pip install praw` (reddit api access)
2. `pip install disqus-python` (disqus api access)

USAGE

1. _env\Scripts\activate

Then to fetch some initial disqus data:

1. Edit ir-config.ini and enter required values.
2. python update.py disqus

To generate a website with dynamic data (recent comments on homepage..):

1. python update.py website

To generate a website with no dynamic data (recent comments on homepage..)

1. python gensite.py

Note that editing either of these scripts might be required, in order to set the right website target.

  TARGET_WEBSITE (generates the website without google analytics and disqus comment sections on articles).
  TARGET_WEBSITE | FLAG_ONLINE (generates the website with google analytics and disqus comment sections on articles).

SAMPLE ir-config.ini contents

[disqus-keys]
private: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
public: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

[paths]
data-storage: .
