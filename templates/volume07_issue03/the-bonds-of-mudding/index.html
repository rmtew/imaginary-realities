{% extends "issue/article/index.html" %}
{% block page_title %}
	The bonds of mudding
{% endblock %}
{% block article_authors %}Clint <span class="author-nick">Itan Kuranes</span> Knapp{% endblock %}
{% block article_date %}10th May 2015{% endblock %}
{% block article_content %}
    <p>
Let's take a moment to consider this world of mudding for a minute. MUDs come about by an act of creative inspiration shared by people with a common interest. We all know a good MUD has a whole host of technical requirements that must be filled: the codebase on which it runs, the area design that makes it unique in a sea of similar code bases, and the people who come together as family to make it all happen.
    </p>
    <p>
It's that last group I want to talk about.
    </p>
    <p>
Whether formed by a core group of friends who met in real-life or some other online forum, a MUD is as much about the people and the motivations that drive them to keep going as it is the size of the world, depth of immersion, or playability of the code itself. That handful of real people, often separated by miles, leagues, and countries are the heart of the MUD itself.
    </p>
    <p>
Once upon a time, in a dark age known as the late 1990s, a core group of online friends came together around their love of the Wheel of Time series by Robert Jordan and set out to create a MUD they eventually named <span class="gametitle">As The Wheel Weaves</span>. They took a stock DIKU/ROM codebase and began to modify it to fit their vision of that world, and it was to this world that I first set down on my twelve year journey through mudding.
    </p>
    <p>
Time and the stresses of ever more complex personal lives took their toll on <span class="gametitle">AWW</span> and eventually the administration changed hands, the MUD being renamed to <span class="gametitle">Prophecies of the Pattern</span>. In those days I saw at a distance how the interpersonal connections between that initial group of friends changed and evolved. Some fell into relationships with one another that went beyond the text world they'd created, and some of those relationships broke down with disastrous results, but through it all many more people came and went who'd joined in on the fun only to find themselves embroiled in some of the deepest and most personal ways imaginable.
    </p>
    <p>
In this world of social media, dating sites of every flavor, and the unending cycles of human relationships spawning from the various ways the Internet can bring people together and tear them apart, this may seem like common sense now, but in the pre-Facebook era this was something of a new world for a guy like me. It's hard to look back on how a simple love of a book series and a few thousand “rooms” of mobs to kill could cause some of the most interesting and compelling relationships in a person's life to happen, but they do happen. Every day.
    </p>
    <p>
Mudders, by and large, are a strange breed of people who for one reason or another seek escape from their daily lives to live in a fantasy world for a time where they can be whoever they want to be. Be it the housewife mother of three who needs a break now and then from parenting, the brooding wannabee writer who seeks an audience of like mind, the quiet nerd coder who wants to experiment with new ideas and hone their craft in a practical application, or the outgoing teenager looking to make some new friends who can share their love of any given world, these are the people who make up the worlds we all enjoy and keep coming back to year after year.
    </p>
    <p>
MUDs provide an outlet; plain and simple. They allow us to become someone else, to interact with people we might never meet in the real world, and to come together to create something everyone can share in. Even when coders fail to live up to expectations or the leader of an important guild disappears into the swamp of real-life drudgery, the MUD goes on. New people come in to share their talents and in doing so form a bond with one another over the game they love.
    </p>
    <p>
Often that bond flows easily into sharing their daily lives and dreams as the anonymity of their character names allow them to speak more freely of the things they hope for and the things that trouble them. Some of these people will spend their entire careers hiding behind those assumed names or only sharing the truth of their identify with a select few people who they've come to spend the most time with. Others will freely express themselves and their identities with anyone who will listen.
    </p>
    <p>
I was one of the former for many years. Even the people I'd spend the majority of my time talking to about game things, book things, loves and interests, and the hardships of any given day were just names on a screen for a long time. Even those closest to me who I thought knew me better than almost anyone else were by and large only character names. Even to this day I think of some of them in character-name terms just out of habit even though those names have changed several times over the years.
    </p>
    <p>
It's just a fact of MUD life, really. We use the names and images we build of one another more readily than trying to picture the person on the other end of the connection. It happens to everyone and should be easy to understand in this age of screen-names and Twitter handles, but MUDs predate those things by decades and the relationships that form over them tend to stick with us for many years.
    </p>
    <p>
Eventually in the MUD world those cliques of close-knit friends might decide to come together to meet up once every year or so just to hang out and enjoy one another's company in the real world, but it doesn't have to happen because the MUD is always there. The MUD doesn't care who has money to travel, who has the bigger house to meet in, who can make it on any particular date; the MUD just exists all the time as a ready communication tool to sit around as long as one can get a stable Internet connection.
    </p>
    <p>
Mudders around the world spend countless hours interacting with one another; hours that if they tried to achieve in the real world would likely lead to a bunch of people sitting around doing nothing else with their lives but talking to each other. Maybe they're only interacting superficially. Maybe the most you'll ever get out of a person is a game stat or a sharp dagger in the back before they run off cackling after looting your corpse. Maybe, though, you'll find someone you connect with on a level so deep and understanding that you'll call them your best friend. Maybe even you'll meet that person in real life and date. Maybe you'll even get married.
    </p>
    <p>
It happens all the time, you know. Even before <span class="gametitle">EverQuest</span>was reaching new heights of online relationship-building and causing entire groups of people to have mass weddings because they'd met that special someone in the game, MUDs were there doing the same thing on a smaller, less visible scale. With nothing more than words.
    </p>
    <p>
What words they can be, though!
    </p>
    <p>
With a simple “How's it going?” an entire conversation might arise between two people who never had anything to do with each other that can find one spending hours recounting all the trials and tribulations of their lives. I can't count the number of interesting things I've learned with those three words alone, and some of the most amazing and fulfilling relationships I've ever had have arisen from just taking the time to ask the person behind the character how they're doing.
    </p>
    <p>
Maybe the next time that high-level rogue sneaks up and stabs you in the back and makes off with some of your hard earned loot you could just ask him those three words. Maybe he'll tell you he's been laid off and is feeling depressed and taking it out on you makes him feel better. Maybe he'll just cackle and run away again, but even if he does: try to remember he's still a person and that how you react to him in any given moment might set off a lifetime of closeness or hate that will shape the way you both interact for years to come.
    </p>
    <p>
It can, of course, be incredibly difficult to really get a sense of a person by the way they type. Words on a screen don't really convey the powerful emotions that drive a person to express themselves in all the myriad ways we do every day. The lack of emotional subtext can often be supplemented to a degree with the omnipresent emoticon or the MUD's in-game social actions, but the fact remains you might not be coming across in the way you intended to.
    </p>
    <p>
When this happens, we fight. Simple as that. People are very emotional beings and anything can set them off. Gauging this reaction is often difficult unless those people have spent a considerable amount of time getting to know each other by the way they speak, the actions they take when they do, or that nasty little web of smiley faces and frowns and winky faces and smirks.
    </p>
    <p>
Mudders, however, always manage to find a way to express themselves more completely. Each person is different in how they'll express themselves, but the longer a MUD runs the more likely a sort of pseudo-language will develop between players becomes. In my MUD of choice, for example, we have a habit of describing actions within a bracket structure as we speak. It grows out of the MUD's own coded socials and often might only describe a word of a given social, but that little bit of action might denote an entirely different meaning than the text of what is said might normally allow.
    </p>
    <p>
Consider a little theoretical exchange:
    </p>
    <p class="quotetext">
      <span class="code">Aes Sedai Ali says oocly, 'How's it going?'</span><br/>
      <span class="code">Dreadlord Dan says oocly, 'It goes. You?'</span><br/>
      <span class="code">Aes Sedai Ali says oocly, '-flops dramatically- it goes.'</span>
    </p>
    <p>
Dan's not giving any real information, but Ali's added that little bit of flair to show that she's probably exhausted, mentally or physically, and wants Dan to know it. Dan can take the bait and go on to ask more and probably get a pretty in-depth answer, or he could just throw out a game social of comfort or polite chuckling and they'll just go on as usual. Maybe they'll get into a discussion of the next time they're going to roleplay a scene together, or go out leveling, or try each other in a fight. Maybe, though, they'll spend the next six hours until sun-up delving into the issues that Ali has to deal with in her life. Maybe they'll become fast friends because of it. Maybe it'll take years to even get around to telling each other their names or what they do, but that little moment of sharing a peek behind the character? That's a powerful moment.</span>
    </p>
    <p>
That tiny glimpse is the MUD equivalent of catching a stranger's eye in a bar and deciding to talk to them based on their body language or the way they're checking you out. It's bumping into someone you only knew on the periphery in high school and deciding you'd been wrong about them all along and want to know more about their life and dreams. It's being introduced by a mutual friend and discovering the love of your life was just a connection or two away. That little thing is the foundation on which a lifetime of friendship might build.
    </p>
    <p>
And boy do they!
    </p>
    <p>
Do you remember the moment you met your first real friend in your game of choice? I do.
    </p>
    <p>
I sat around in the bank in the city of Caemlyn (our equivalent to a central hub through which everyone comes and goes around the world) just waiting those ten agonizing minutes between chances to request a quest to get up the quest points to buy a new skill that would raise my stat cap to twice its normal level. A Mentor in the game came in, an Aes Sedai of the Yellow Ajah, and asked how it was going. I don't know what happened after that exactly. I probably spent a lot of time asking some basic questions, but the end result was the same: that line of communication was open.
    </p>
    <p>
We'd talk from time to time about game stuff or theories about where the series was going, or complain about our relationships in the real world, and that bond just formed and grew. We had a lot in common, but a lot of differences too that made us interesting to each other, and twelve years later? I talk to that person every day and consider her my best friend. I can't imagine a world where the random chance of my computer lab time coincided with her logging on and just hanging out in the game didn't happen. Even without ever meeting in real-life, that person became closer to me through the random wanderings of our conversations and sharing the ups and downs of our lives than many of my oldest friends in the real world.
    </p>
    <p>
That's the power of words, and it happens all the time. It's only even one example. I could go on for hours about the friends I've made who became a second family to me; or the ones I thought were going to be important and lost contact with overnight in a single flurry of angry ranting. There are people I can't imagine not being a part of my life because we came together over a singular shared interest in this archaic little world of text-based gaming. Those people are the core around which our game has continued to persist, but even if the game went offline tomorrow they'd remain a deeply important part of my life and continue to inspire me with the openness with which they share their lives.
    </p>
    <p>
I'm not alone, either.
    </p>
    <p>
Everyone reading this has probably got countless stories of that best-MUD-friend who they can't go a day without sharing their lives with. Some of you probably even married those people. Some might have married and divorced and married someone else they found through the game you play. Some might even be bitter enemies with someone they met in a MUD and spend every day seething at the merest appearance of <span class="command">“*** Soandso joins in looking for some adventure.”</span> across the black space between two hit-point-prompt lines.
    </p>
    <p>
Whatever the case may be, the relationships we form as mudders who come together over a shared interest can be as real and fulfilling as any other in life. Perhaps you're an introverted, chronically depressed person in life who just needs a shoulder to cry one from time to time in the relative safety of anonymity. Perhaps you're just looking for someone to spend a little time having fun with and then go about the rest of your life never really giving that other person another thought. Perhaps you'll even fall in love and move halfway across the globe to be with that special person you spent so many hours talking to and getting to know.
    </p>
    <p>
You never know unless you take that chance and ask those three simple words: “How's it going?”
    </p>

{% endblock %}
{% block article_bio_content %}
	Itan Kuranes (Clint Knapp) is a 14-year veteran of Prophecies of the Pattern and its predecessor As The Wheel Weaves, and blogs at <a class="publicationtitle" href="http://clintknapp.wordpress.com">clintknapp.wordpress.com</a>.
{% endblock %}
