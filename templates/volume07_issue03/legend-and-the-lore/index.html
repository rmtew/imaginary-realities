{% extends "issue/article/index.html" %}
{% block page_title %}
	Legend and the lore
{% endblock %}
{% block article_authors %}<span class="author-nick">Hugo Zombiestalker</span>{% endblock %}
{% block article_date %}3rd May 2015{% endblock %}
{% block article_content %}
    <p>
      I'm convinced that point-and-click games are a thing of the past. For a long time I held out hope that they weren't an out of date design, that the world simply favoured action oriented games. But with the string of faithfully recreated point-and-click adventures, such as <span class="gametitle">The Whispered World</span>, <span class="gametitle">Broken Age</span> and <span class="gametitle">Dead Synchronicity</span>, I've come to the conclusion that the glory days of adventure games are long gone, and rightfully so.
    </p>
    <p>
      There remains something about this genre that intrigues me, though, something that steps outside the realm of merely being a genre from the past. There are a handful of classic adventures that can still hold my attention, but outside of their themes and beautiful artwork (<span class="gametitle">Escape from Monkey Island</span>, <span class="gametitle">Grim Fandango</span>) I think it’s because I know all their puzzles and I can jump straight into the world and narrative. I will speak about this a bit more below.
    </p>
    <p>
      Historically, adventure games probably followed the puzzle narrative because no one else had thought of a better reason to advance the story. Game development has opened many doors since then, and many adventure game developers have gone in only two directions: give up entirely, or plow ahead with the same archaic game design. I’m not an open critic of archaic ideologies (quite the opposite), but if the knowledge base around the mechanics of a thing, such as the adventure game, progresses over time, then naturally that thing should, if its nature allows it, progress to a better state.
    </p>
    <p>
      The point-and-click adventure game is dead, and I’ve seen now that it cannot be recreated in its original format without appealing to the early crowd; moreover, point-and-click games are dusty and creaky, and the greats today are merely mimicking a style that was dusty back then, too, we just couldn’t see it. I don’t pretend to know the direction this genre should take, only that to have a future as something more than a retro niche, it needs to adapt alternative advancement within its undoubtedly fascinating framework.
    </p>
    <p>
      This article talks about what interests me about adventure games and how they have inspired my creative writing and online game development, and how new MUD mechanics have arisen from this.
    </p>
    <p>
      So what is it about point-and-click adventures that attract me, and why is it relevant to modern game design? Let's take a look at a small list of elements I think are important to these types of games: story, atmosphere, and dialogue.
    </p>
    <p>
      You may wonder why I haven't included “puzzles” on that list. The truth is, that's a debatable question. I'm sure there are hardcore fans out there that flock to adventure games for their puzzles, or that they are advertised for their puzzles, but in fact I've never really been attracted to them 
      <span class="italicd">because</span> of the puzzles. They are a by-product, simply an obstacle in order to advance the story, as noted above. If you want true puzzles, then there is an entire game genre dedicated to that, so I think puzzles in adventure games aren't a true drawing point, and that they existed and continue to exist purely as a way to move the game along.
    </p>
    <p>
      Growing up, I was a point-and-click kid. I wasn't much interested in anything other than simulation games and adventure games. When I played adventure games, I constantly had the walkthrough by my side. This thought process stayed with me as an <span class="gametitle">Epitaph</span> developer. I'm not a fan of throwing huge amounts of text in the players’ faces and expecting them to read it. Games like <span class="c2">Dragon Age: Origins</span> and <span class="gametitle">Pillars of Eternity</span> are notorious for this, and I think it's a distraction, maybe even lazy writing (the dialogue succeeds at being voluminous while failing at being interesting). That's where game mechanics come in, and why it's important to slowly trickle information to the player through a variety of means.
    </p>
    <p>
      What part of MUD design uses the old point-and-click formula? Well, the quest system for one. The quest system is a form of this game design. There are some who view quests as the “puzzle” system of the game, and while I don't disagree with this, for me it is a continuation of the classic adventure. I'm not puzzle-minded. In fact, I'm terrible with puzzles, for both designing and solving. When I make quests I try to focus on what I believe is important: the story. There are varying thoughts on how best tell a story, but in games it's nearly always important to tell them through meaningful activities, similar to how the Communicative Approach in ESL teaching handles the learning of English. Below I will outline what I believe is important to quest design. Before continuing, think about what you consider is important, and see if my list matches yours.
    </p>

    <h3>Story</h3>
    <p>
      Story is arguably the most important part of the quest. Even the most heavily puzzle-oriented quest, where one may not even think the story is important, has a story. It can be as simple as flicking a switch on a breaker (because you want to try and restore electricity to a survivor stronghold) or as complex as sending a message using bells in a church. Both of these quests utilize a story, a motivation, an objective, no matter how easy or complicated, and yet the quest puzzles are miles apart — the first hardly even being considered a puzzle.
    </p>

    <h3>Atmosphere</h3>
    <p>
      Atmosphere should never be a conscious thing. It’s a difficult thing to create and I believe it takes a certain amount of skill. The player should never be thinking about this; it should simply enter the subconscious and elicit an emotional response. Classic horror used atmosphere to drive the plot. If you've ever read Algernon Blackwood, you would know that his stories are all about the atmosphere. This is an underestimated point in quest design and one that should never be omitted. I will provide an example, by Mr. Blackwood himself, from <span class="publicationtitle">The Willows, Algernon Blackwood</span>:
    </p>
    <p class="quotetext">
      “The melancholy shrill cry of a night-bird sounded overhead, and suddenly I nearly lost my balance as the piece of bank I stood upon fell with a great splash into the river, undermined by the flood. I stepped back just in time, and went on hunting for firewood again, half laughing at the odd fancies that crowded so thickly into my mind and cast their spell upon me. I recalled the Swede's remark about moving on next day, and I was just thinking that I fully agreed with him, when I turned with a start and saw the subject of my thoughts standing immediately in front of me. He was quite close. The roar of the elements had covered his approach.”
    </p>
    <p>
      Here’s another one from Mr. Blackwood, from a John Silence novel called <span class="publicationtitle">Ancient Sorceries</span>:
    </p>
    <p class="quotetext">
      “As once before, he saw her tall and stately, moving through wild and broken scenery of forests and mountain caverns, the glare of flames behind her head and clouds of shifting smoke about her feet. Dark leaves encircled her hair, flying loosely in the wind, and her limbs shone through the merest rags of clothing.”
    </p>
    <p>
      Powerful. His tension is presented to us through the environment, and the actual horror is conjured by our own minds. This is the best approach for purposes of atmosphere.
    </p>

    <h3>Dialogue</h3>
    <p>
      Dialogue is not nearly as important for MUD quests as it is in adventure games, but if you've got a quest where there's dialogue, it needs to be impactful. I think mild humour is important even in the most serious environment. Don't let dialogue slide, and don't think you should remove humour simply because your story is dark and gritty. Humour is natural, and it helps bring players into your world. It's sort of the masseuse your mind needs in order to get through the really hard stuff. The reverse is also true. I read a book on how to write humour, and a key feature was about presenting serious situations and letting ridiculous things happen. The film <span class="publicationtitle">Death at a Funeral</span> (UK) is a great example; it's a black comedy, but the same idea applies. The serious situation: the funeral. The humour: pretty much everything that is said. Here’s an excerpt from the video game <span class="gametitle">Grim Fandango</span> that mixes comedy with the macabre:
    </p>
    <p class="quotetext">
      Hector LeMans: Oh Manny... so cynical... What happened to you, Manny, that caused you to lose your sense of hope, your love of life?<br/>
      Manuel Calavera: I died.
    </p>

    <h3>Puzzles</h3>
    <p>
      It's important to be clear what I mean about a “puzzle”. A puzzle is simply the obstacle you need to overcome to complete the quest. Everything is going to be a puzzle, more or less. But my quest philosophy takes in the three points above <span class="italicd">first</span>. Once I have a story, atmosphere, and if needed, dialogue, then the puzzles are laid out. My puzzles have included things as simple as finding an object in a room, ranged from there to mildly complex tasks like finding the correct name of a guest at a hotel in the registry book in the lobby. I think quests can definitely follow the old pattern of simply exploring and finding and discovering, if the above points are all done well.
    </p>

    <h3>Natural progress and the Z-Files</h3>
    <p>
      While designing quests on <span class="gametitle">Epitaph</span>, and writing lore, and working with the various factions and world-building and characterizations, I came to the conclusion that a new system needed to be installed, one that bridged the gap between “achievements” and “quests”. I don't always think a good story means you should make a quest out of it, but you still need interaction in order to make it meaningful. What I needed was a way to make story elements important and rewarding, but easier to create than a full-blown quest. Thus, the codex system was born.
    </p>
    <p>
      The codex system on <span class="gametitle">Epitaph</span> is a way to present our lore to the players in a rewarding way. It's important to keep the game interesting through dynamic mechanics. Being an apocalyptic survival horror game, there is ample room for survivor tales, background information of places and buildings, and other story interests. I wanted a way to make players feel that the lore was important, that it wasn't just there or not there. I wanted to bring lore and story back to the forefront, and it's not as simple as saying “this place has a lot of story: people <span class="italicd">died</span> here!” Who cares? But what if we let players build the pieces together themselves, from finding old items, diary pages, even biological substances that can be researched? How about putting in some emotional value to the story, so that while the story is said and done, it can be presented through dedication by the player?
    </p>
    <p>
      Part of this inspiration came from old horror novels where characters believed items contained emotion from those who owned them. Some of the codex system is about items in the game world retaining a memory of what happened to the place where they existed or who had possession of them. Also, the game <span class="gametitle">Murdered: Soul Suspect</span> has a vague mechanic called “Ghost Stories” that is similar in function to our codex system; however, I feel their system could have been much bigger than it really was. I’m not usually a collector in games, but I completed nearly all the collection achievements in that game (including all the Ghost Stories) just because of a few of the points I talked about above — atmosphere and setting being a big plus. So in some ways the codex system is a continuation of a simple yet interesting system used in said game. (Incidentally, <span class="gametitle">Murdered: Soul Suspect</span> is overpriced but underrated. If you can pick it up for less than €10, grab it and play it. It’s very interesting. Just don’t expect combat.)
    </p>

    <h3>How does it work?</h3>
    <p>
      The codex system is quite simple mechanically. There are X amount of entries per “collection”. When you discover and unlock all the entries of a collection, you gain rewards that can include faction reputation, titles, experience points, and so on. Each entry the story) varies in length, so if a story contains four stages, you must unlock that entry four times to get the complete story. You find and loot items from around the game world (usually in the location stated in the entries’ category) and use these items to unlock the narrative. Let's take a look at the entry <span class="italicd">The Foul Within [3/3]</span>. As you can see, it has three stages. The material below is from the first stage, and it tells us what is required to unlock the next part:
    </p>
    <p>
      <span class="boldd">Entry:</span> The Foul Within<br/>
      <span class="boldd">Text:</span> It always starts off bad and turns worse. Some could say my case started worse and turned bad. Who can tell? We've all lived and lost in this crazy thing we call the apocalypse, but one thing we can all agree on is this: some lose more than they deserve, and others live more than should. When I looked at the remains of my three year old daughter, I knew then that life was never the way I had imagined it, and that for all my years I had been living behind an illusion illuminated by spotlights and shadows on the walls projected by invisible creatures none of us would ever like to see.</br>
      <span class="boldd">Next unlock:</span> 1x sleeping pill with a quality of misc, 1x faded photograph and 2x diary entry with a quality of misc.
    </p>
    <p>
      Each unlock reveals the next part of the story until all the story has been revealed. The collection categories are quite varied, and at the moment contains categories from the main game areas (Dunglen, Braellen, Silent Hollow), but also from our factions, as well as some general categories. I hope to use this system for talking about background stories of world bosses, and as the main platform to advance faction-based stories that may be critical to the game as a whole. At the moment it's still in the early stages and will release with only a select few collections, but it will be something that certainly will be expanded in following patches.
    </p>

    <h3>Conclusion</h3>
    <p>
      My hope is that every new game area added will contain a few of these codex stories, which currently range from poems to flash fiction, and hopefully one day will include short story serials. I'm not intending to replace background storytelling (for example, stumbling upon a cassette tape from some mad scientist); it's merely to give focus to certain stories that lie around the game, to build a bigger picture through actual game activity.
    </p>
    <p>
      At the time of writing, the codex system has not yet been released to <span class="gametitle">Epitaph</span>, but I hope that it is received well, and that players will get to understand the world they're playing in, and see that it's not simply kill this, die to that, that there are other lives involved, lives that have perished, just like your own will one day. Mechanically, we’re talking about a “collecting” system, and that’s okay. I’m a follower of the notion that it’s not all about uniqueness or unnecessary complication, but about heart and soul. The great James Lee Burke said that the best stories are the ones told by the heart, and I believe that. This system isn’t just a gimmick thrown in to please a certain crowd — it’s a system I want to use to express the lore side of a game. I’ve already got notes on how to expand this system further, but for now let’s see just how the first incarnation works out.
    </p>
{% endblock %}
{% block article_bio_content %}
	Hugo Zombiestalker is a senior developer on <a class="gametitle" href="http://drakkos.co.uk">Epitaph</a>.
{% endblock %}
