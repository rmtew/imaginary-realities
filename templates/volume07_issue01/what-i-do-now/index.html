{% extends "issue/article/index.html" %}
{% block page_title %}
	What Do I Do Now
{% endblock %}
{% block article_authors %}Richard <span class="author-nick">KaVir</span> Woolcock{% endblock %}
{% block article_date %}2nd June 2014{% endblock %}
{% block feature_excerpt %}
    From time to time I&#39;ll try out new MUDs, yet after a short while I often find myself wondering &ldquo;What do I do now?&rdquo;
    &mdash;
    &nbsp;and that&#39;s usually the point at which I start feeling bored or frustrated, and quit. I&#39;ve also had newbies on my own MUD ask that very same question, and when answers weren&#39;t forthcoming they&#39;d frequently quit as well.
{% endblock %}
{% block feature_hype %}
	Most MUDs immediately confront a new player with permanent choices like their race and class, as soon as they create their character.  This might be followed by a short tutorial, after which the player is left on their own to scratch their head.  But there is a better way..
{% endblock %}
{% block article_content %}
   <p class="quotetext">Virtual worlds live or die by their ability to attract newbies</p>
   <p class="quoteattr">Richard Bartle, 3rd November 2004</p>

  <p>
    From time to time I&#39;ll try out new MUDs, yet after a short while I often find myself wondering &ldquo;What do I do now?&rdquo;
    &mdash;
    &nbsp;and that&#39;s usually the point at which I start feeling bored or frustrated, and quit. I&#39;ve also had newbies on my own MUD ask that very same question, and when answers weren&#39;t forthcoming they&#39;d frequently quit as well. Although the newbie can always use public channels to ask for help, that relies on other people being online, active, sufficiently knowledgeable, and willing to help.
  </p>

  <h3>The Window of Opportunity</h3>

   <p class="quotetext">I think people who have played a lot of muds know very quickly if a game will interest them. If you have something really different that you want to bring to people&#39;s attention it needs to be there right away.</p>
   <p class="quoteattr">Ide, 24th June 2010</p>
  <p>
    As many MUD owners have observed over the years, there&#39;s only a very small window of opportunity in which to convince newbies to stay. If you don&#39;t capture their interest within the first few minutes, most of them will simply quit and try another MUD.
  </p>
  <p>
    Of course this is where things like streamlined character creation come in
    &mdash;
    &nbsp;why it&#39;s advantageous to bring the new player into the MUD as fast as possible, before they start getting bored. But getting them to complete character creation is only the first step. Once they&#39;ve landed in the game, one of the worst things you can do is force them to read page after page of dry text, on their own, without any interaction with other players.
  </p>
  <p>
    You want to get the newbies interested and excited about your MUD, not force them to jump through a set of hoops! But you also need to make sure they know what they&rsquo;re supposed to be doing, otherwise that interest and excitement will rapidly turn into frustration.
  </p>

  <h3>Newbie School, or Newbie Prison?</h3>

   <p class="quotetext">I would rather put the player into a normal city and give them directions to various quest chains that teach them how to play in the actual real environment they&#39;ll be spending a lot of time running around, rather than some preschool style candyland that will bore them to death.</p>
   <p class="quoteattr">Quixadhal, 31st May 2014</p>
   
  <p>
    A traditional staple of many MUDs is the newbie school
    &mdash;
    &nbsp;a learning area where new players first spawn (frequently in just their underwear!), and can learn how to play before entering the Big Bad World. The newbie school typically involves learning and using various basic commands, training certain skills or other abilities, defeating a few weak opponents, and collecting some crude newbie equipment.
  </p>
  <p>
    However, this approach also tends to isolate the newbie from the rest of the community
    &mdash;
    &nbsp;in extreme cases the newbie is completely blocked off from the rest of the MUD until they&#39;ve &ldquo;graduated&rdquo; from the school.
  </p>
  <p>
    Another major problem is that newbie schools are generally a &ldquo;one size fits all&rdquo; solution, while the actual newbies themselves have widely varying expectations and needs. A more experienced player might work their way through the school relatively quickly and find it a boring chore, while a complete beginner might get confused or stuck, and need to ask for additional help.
  </p>

  <h3>Newbie Tutorials</h3>

   <p class="quotetext">There are people who complete the tutorial, then face a quest-giver in the city, spam 100 kill attempts, and then quit...</p>
   <p class="quoteattr">Plamzi, 28th June 2013</p>
   
  <p>
    Another approach used by some MUDs is to offer a tutorial. Whereas the newbie school is a special location, the tutorial is more like a guide
    &mdash;
    &nbsp;in some cases
    literally
    &nbsp;an NPC guide. Although it&#39;s possible to combine both approaches (i.e., a tutorial within a newbie school), one advantage of a tutorial is that it doesn&#39;t
    need
    &nbsp;to confine the newbie to a specific corner of the MUD; it can lead them wherever they need to go, throughout the rest of the game world, instead of locking them away from the rest of the community.
  </p>
  <p>
    Tutorials still tend to suffer from the cut-off issue, perhaps even more so than the newbie schools; if you&#39;ve been guided step-by-step through the basics, it&#39;s not very nice to find yourself being suddenly dropped into the middle of town among the unwashed masses, with no idea what you&#39;re supposed to do next.
  </p>

  <h3>Kicking Out the Crutch</h3>

 <p class="quotetext">I have no idea what to do from here. Interest lost, it was great up until this point, but there is no obvious direction to continue.</p>
 <p class="quoteattr">Donky, 11th February 2010</p>
 
  <p>
    Even if you have an interesting and interactive newbie tutorial, for many players it can be quite a jarring experience when it ends, as they&#39;re suddenly moved from a linear guide into a sandbox environment. In effect, you&#39;re saying &ldquo;now it&#39;s time to walk on your own&rdquo;, and then kicking the crutch out from under them, without bothering to ask if they&#39;ve learned to walk without it.
  </p>
  <p>
    This can be a risky time for player retention, because the newbie is jumping straight from one style of gameplay (linear and guided) to another (sandbox and freeform) without any warning. Even if you&rsquo;ve given the player the skills they need, and they&rsquo;ve managed to remember everything they&rsquo;ve been told, there&rsquo;s still going to be a moment of panic and confusion when they take the plunge.
  </p>
  <p>
    One obvious solution is to let the player
    decide
    &nbsp;when they want to discard the crutch, so that at least they&rsquo;re the one calling the shots. But even that isn&rsquo;t perfect, as the newbie may not know what they&rsquo;re getting into &mdash; and even if they&rsquo;re confident early on, they may later decide that they need help again.
  </p>

  <h3>Passive Tutorial Mode</h3>

   <p class="quotetext">It does make me wonder if you can make the whole game a tutorial. Perhaps have a tutorial mode. I wouldn&#39;t want to dump players out of an experience that is guided, to one that is completely freeform.</p>
   <p class="quoteattr">Donky, 11th February 2010</p>
   
  <p>
    Almost all MUDs have a &ldquo;who&rdquo; command, to show who is currently online. Some MUDs also have a &ldquo;where&rdquo; command, to show where people or things are. These commands are intuitive and very useful &mdash; so I thought to myself, why not add another? Why not have a &ldquo;what&rdquo; command, to provide suggestions for what to do next? In effect, a passive tutorial mode that players can fall back on whenever they&rsquo;re feeling confused or are unsure what to do next, tailored specifically to the individual.
  </p>
  <p>
    Of course the first step was to make sure newbies knew that a &ldquo;what&rdquo; command was available, as it&rsquo;s only intuitive once you know about it! I therefore decided to borrow an idea from Realms of Despair, and include the information in the prompt:
  </p>
  <div class="imgcenter">
   <img class="center" alt="A picture." src="images/image00.png" width="601" />
  </div>
  <p>
    Some might consider it slightly intrusive, but it&rsquo;s a compromise that pretty much guarantees newbies will type &ldquo;what&rdquo; (or click it
    &mdash;
    &nbsp;the underlined text is clickable, another technique which is intuitive for many newbies). After typing &ldquo;what&rdquo; for the first time, the prompt reverts to normal, and the newbie should know how to access the tutorial mode in future.
  </p>
  <div class="imgcenter">
    <img class="center" alt="A picture." src="images/image01.png" width="601" />
  </div>
  <p>
   <span class="c0">
    The early phase of the game proved fairly easy to cover, as it was already relatively linear and broken up into clear objectives. All I really needed to do was push the player towards their next objective and give them some suggestions for how to overcome it. I threw in a few other words of wisdom along the way, but tried to provide a single clear goal for each step.
   </span>
  </p>
  <div class="imgcenter">
    <img class="center" alt="A picture." src="images/image02.png"  width="601" />
  </div>
  <p>
    Although the early part of the tutorial was still fairly linear, it was also the most important, as it had to provide accurate information and tailored suggestions for players who weren&rsquo;t yet familiar with the MUD.
  </p>
  <div class="imgcenter">
    <img class="center" alt="A picture." src="images/image03.png"  width="601" />
  </div>
  <p>
    But I didn&rsquo;t want the tutorial to have a cut-off point, I wanted to provide a tool that would continue to be useful to players even after they&rsquo;d completed the newbie phase of the game, and had moved on to exploring the main world and completing major quests. I didn&rsquo;t want them to
    wonder
    &nbsp;what they could do next
    &mdash;
    &nbsp;I wanted them to type &ldquo;what&rdquo; and find out!
  </p>
  <div class="imgcenter">
    <img class="center" alt="A picture." src="images/image04.png"  width="601" />
  </div>
  <p>
    Even powerful PCs who had completed most of the content might wonder what else they could do, particularly if they&rsquo;d been away from the MUD for a while.
  </p>
  <div class="imgcenter">
    <img class="center" alt="A picture." src="images/image05.png"  width="601" title="" />
  </div>
  <p>
    Because the &ldquo;what&rdquo; command is useful for everyone, people rarely forget it &mdash; but just in case, I decided to tie it in with the auto-help feature as well.
  </p>
  <div class="imgcenter">
    <img class="center" alt="A picture." src="images/image06.png"  width="601" />
  </div>
  <p>
    Newbies are the lifeblood of an active MUD, but if you want them to stay you have to make sure their experience is positive &mdash; and that means the
    entire
    &nbsp;experience, not just the introduction. You need to make a good impression as early as possible, but that doesn&rsquo;t mean you should stop putting in any effort once they&rsquo;ve completed the basics.
  </p>
{% endblock %}
{% block article_bio_content %}
	KaVir (Richard Woolcock) is the owner of God Wars II.
{% endblock %}
 