{% extends "issue/article/index.html" %}
{% block page_title %}
	Choosing an Emoting System
{% endblock %}
{% block article_authors %}<span class="author-nick">Griatch</span>{% endblock %}
{% block article_date %}19th May 2014{% endblock %}
{% block article_content %}
  <p class="rcaptionblock">
  <img alt="howl_by_griatch_art-d62b1yl.jpg" src="images/image01.jpg" style="width: 231.50px; height: 337.23px;">
  </p>
  <p>
   <span class="italicd">images from <a href="http://www.griatch-art.deviantart.com">http://www.griatch-art.deviantart.com</a></span>
  </p>
  <p>
    The act of <span class="italicd">emoting</span>, or <span class="italicd">posing</span>, is an important part of many online text games, but the way emoting works varies quite a bit between games. This article lists and discusses some common options. It is mainly aimed at those interested in implementing their own game.
  </p>
  <p>
    An emote should not be confused with other forms of in-game communication such as tells and channel messages. An &ldquo;emote&rdquo; is here defined as text displayed as originating from the in-game character only (usually only visible to those in the same game location).
  </p>
  <p>
    When deciding on which emoting system to go for, it is important to consider appropriateness relative to the type of game. Emotes traditionally serve different roles depending on game (ordered roughly from least roleplay-centric to most roleplay-centric):
  </p>
  <ul>
   <li>They may act as a form of &ldquo;smileys&rdquo; to convey an emotion, emphasis or personal touch during play.</li>
   <li>They may act as quick ways to communicate in-game information, in-character or not.</li>
   <li>They may act as an actual extension of coded systems (such as being parsed for commands, language or offer in-emote command execution).</li>
   <li>They may act as a filler between what a character<span class="italicd">should realistically</span>&nbsp;be able to do and what the game&rsquo;s code <span class="italicd">actually</span> allows the character to do. In some games this is only window dressing; in others, actions done in an emote are just as binding as an action done &ldquo;in code&rdquo;.</li>
   <li>They may be the <span class="italicd">only</span> legal way to affect the game world, such as in many MUSH-style games.</li>
  </ul>

  <h3>Actor versus Director stance</h3>
  <p>
    You are emoting that you are standing in a bar. Consider the difference between these two emote returns:
  </p>
  <p>
   <span class="boldd">1. You see:</span>
   <span class="italicd">You are standing by the bar, your eyes narrowed as you look around.</span><br/>
   <span class="boldd">&nbsp; &nbsp;</span>
   <span class="boldd">Others see:</span>
   <span class="italicd">Griatch is standing by the bar, his eyes narrowed as he looks around.</span>
  </p>
  <p>
   <span class="boldd">2. You see:</span>
   <span class="italicd">Griatch is standing by the bar, his eyes narrowed as he looks around.</span><br/>
   <span class="boldd">&nbsp; &nbsp;</span>
   <span class="boldd">Others see:</span>
   <span class="italicd">Griatch is standing by the bar, his eyes narrowed as he looks around.</span>
  </p>
  <p>
    (Just how the emote command syntax looks for each case is irrelevant at the moment.)
    The first of these is an example of <span class="italicd">Actor stance</span>&nbsp;&mdash;&nbsp;you (the Player) &ldquo;are&rdquo; the character. You see and hear what the character experiences. The second represents <span class="italicd">Director stance (</span>or &ldquo;Author stance&rdquo; depending on how strict you are with definitions). In the latter you are seeing your character from the &ldquo;outside&rdquo; and control it as a sort of puppet master or director. Apart from the obvious use of second-person &ldquo;you&rdquo;, there are some deeper philosophical differences between the two.
  </p>
  <p>
    Most MUD-style games tend to use some variant of actor stance whereas MUSHes and other storytelling-focused games tend toward director stance. Many games also confuse the issue by using second-person form for coded messages (&ldquo;You feel hungry&rdquo;) even though roleplay is otherwise done in director stance.
  </p>
  <p>
	<img class="rcaptionblock" alt="Dance_my_Jester_by_Griatch_art.jpg" src="images/image00.jpg" width="232">	
    The actor stance is the traditional stance in tabletop roleplaying. One common argument for using actor stance is that &ldquo;being&rdquo; the character and seeing what they see &ldquo;aids immersion&rdquo;. I personally think that this is a weak argument on its own. Books have been very successful at getting us to feel for and relate to characters without explicitly suggesting that <span class="italicd">we</span>&nbsp;are that character. One could argue that interactive game avatars work differently &mdash;&nbsp;but when your character gets hit by a sword, jumps into a lava pit or has their head bit off by a dragon,
	you generally have no trouble recognizing that this happens to <span class="italicd">someone else</span>&nbsp;and not to <span class="italicd">you</span>. As a social species we are very good at putting ourselves in and out of other people&rsquo;s shoes on demand, using &ldquo;you&rdquo; or not.
  </p>
  <p>
    Vice-versa, the drawback of the director stance is that, taken to the extreme, there is no reason for the game to limit what you as the <span class="italicd">player</span>&nbsp;know, only what your <span class="italicd">character</span>&nbsp;knows. It may put a lot of strain on the player to separate player knowledge from character knowledge.
  </p>
  <p>
    For example, if you go full-director stance, it would make no sense for the game to hide that another character, known to be an assassin, is secretly sneaking up on you. As long as the game makes it clear that your
   <span class="italicd">character</span>
    &nbsp;has no clue (for example by telling you that you failed a detection roll) you are expected to act as though you are oblivious. For a game type where non-consensual character-killing is rare or impossible (such as many a MUSH) this is fine; it just leads to an interesting scene. But for games where a coded backstab could kill your character, this puts a lot of emphasis on the maturity of the player. What if the targeted player suddenly makes their character leave the room? Would they have done so regardless of the assassin, or are they wrongly using their player knowledge to cheat and get their character out of harm&rsquo;s way? It can be very hard to tell. Simply hiding the information from the player as well as from the character sidesteps the problem entirely.
  </p>
  <p>
    Technically, implementing the emote-handling for actor stance is more complex than director-stance parsing. Not only do you have to track who is seeing the text, your parser needs to know how to modify the grammar of your MUD&rsquo;s language dynamically (&ldquo;<span class="italicd">You are&rdquo;</span> versus &ldquo;<span class="italicd">Griatch</span>&nbsp;<span class="italicd">is&rdquo;</span>, for example). In many codebases, the combination of rigid coded systems (which often uses stylistic second-person by default) and director-style emotes leads to a somewhat disparate mix between the two.
  </p>
  <p>
    The debate of whether to use actor or director stance is leaking over from the tabletop roleplaying world. Which to choose is very much a matter of preference, but when implementing your system, at least take the time to consider what you are aiming for and why.
  </p>
  <h3>Emoting Styles</h3>
  <p>
    In the MU* text-based gaming world there are a slew of common forms that an emote command may take. In the rest of this article I&rsquo;ll try to list and discuss some common alternatives. Although I use &ldquo;emote&rdquo; as a command name here (I could also call it &ldquo;pose&rdquo;), the command is so ubiquitous that it&rsquo;s often aliased to a single character like a colon or semicolon.
  </p>
  <h4>Pre-set emote</h4>
  <blockquote>
   <span class="command">&gt; bow</span><br/>
   <span class="italicd">Griatch bows.</span>
  </blockquote>
  <p>
    The pre-set emote produces a given (possibly randomized) output. This basic form of emote is often available even in the most hardcore hack &amp; slash game. It is also the only emote commonly seen in graphical games.
  </p>
  <p>
    Technically, this is by far the simplest type of emote to implement. It could look like this (example Python code for the <a href="http://www.evennia.com">Evennia</a>&nbsp;MUD creation system):
  </p>
<pre class="python-code"><code class="python-code">import&nbsp;ev
    
class CmdBow(ev.Command):
&nbsp; &nbsp; key = &quot;bow&quot;
&nbsp; &nbsp; def func(self):
&nbsp; &nbsp; &nbsp; &nbsp; text = &quot;%s bows.&quot; % self.caller.key
&nbsp; &nbsp; &nbsp; &nbsp; self.caller.location.msg_contents(text)</code></pre>
  </p>
  <h4>Extended pre-set emote</h4>
  <blockquote>
   <span class="command">&gt; bows with a flourish.</span><br/>
   <span class="italicd">Griatch bows with a flourish.</span><br/><br/>
   <span class="command">&gt; say This is a good day.</span><br/>
   <span class="italicd">Griatch says, &ldquo;This is a good day.&rdquo;</span>
  </blockquote>
  <p>
    The <span class="italicd">say</span>
    command is by far the most common form of extended emote, so traditional that it&rsquo;s rarely identified as an emote at all. In some games the separation makes sense due to it being faster to type. In other games this traditional separation is unfortunate: there is no reason why saying something should be conceptually different from emoting (see the Language-parsing section of freeform emotes, below).
  </p>
  <p>
    Offering the ability to expand on a pre-set emote is technically simple. It is also fast to type (skipping the extra info would just give the vanilla pre-set emote).
  </p>
  <p>
    An example in Evennia code:
  </p>
<pre class="python-code"><code class="python-code">import ev

class CmdBow(ev.Command):
    &nbsp; &nbsp; key = &quot;bow&quot;
    &nbsp; &nbsp; def func(self):
    &nbsp; &nbsp; &nbsp; &nbsp; if self.args:
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; text = &quot;%s bows %s&quot; % (self.caller.key, self.args)
    &nbsp; &nbsp; &nbsp; &nbsp; else:
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; text = &quot;%s bows.&quot; % self.caller.key
    self.caller.location.msg_contents(text)</code></pre>
	
  <h4>Commands with emote extension</h4>
  <blockquote>
   <span class="command">&gt; hit tom : with a swift downward blow!</span><br/>
   <span class="italicd">Griatch attacks Tom with a swift downward blow!</span>
  </blockquote>
  <p>
    This is the ability to tack an emote to a non-emote related command. There is little technical &nbsp;reason <span class="italicd">not</span>&nbsp;to offer this in most games since the emote could be skipped completely for speed or used to add immersion and personality when needed. Implementation is mainly a matter of establishing a standard parsing of commands and figuring out just how to tag on the emote (in the above example, the emote is simply created by appending a colon to the end of a normal command). Of course, it helps if the game design takes this into consideration from the get-go.
  </p>
  <h4>Freeform emote, locked emoter</h4>
  <blockquote>
   <span class="command">&gt; emote stands by the bar, looking around.</span><br/>
   <span class="italicd">Griatch stands by the bar, looking around.</span>
  </blockquote>
  <p>
    This is a very common emote type, where the first word is always the name of the emoter. Many parsers also handle things like possessive form (<span class="italicd">emote&rsquo;s</span>&nbsp;&rarr; <span class="italicd">Griatch&rsquo;s</span>). Auto-including the emoter like this has practical reasons &mdash;&nbsp;it allows everyone to know who is emoting even if the emote doesn&rsquo;t reference the emoter explicitly. It is also easy to implement:
	
    <pre class="python-code"><code class="python-code">import ev
class CmdEmote(self):
&nbsp; &nbsp; key = &quot;emote&quot;
&nbsp; &nbsp; func(self):
&nbsp; &nbsp; &nbsp; &nbsp; text = &quot;%s %s&quot; % (self.caller.key, self.args)
&nbsp; &nbsp; &nbsp; &nbsp; self.caller.location.msg(text)</code></pre></p>

	<h4>Freeform emote with dynamic parsing</h4>
  <blockquote>
   <span class="command">&gt; emote Standing by the bar, /self looks around.</span><br/>
   <span class="italicd">Standing by the bar, Griatch looks around.</span>
  </blockquote>
  <p>
    This is an emote that also incorporates a mini-language, here exemplified by the use of the <span class="italicd">/self</span> keyword. Exactly which syntax is used varies, including, among others, single-characters like &ldquo;@&rdquo;, double-commas, &nbsp;<span class="italicd">/me,</span> and <span class="italicd">#self</span>.
  </p>
  <p>
    If the player does not include a self-reference, the name can be auto-appended to the start or end of the emote. Alternatively, the emote can be logged to make sure the author of a given emote can always be identified should the need arise.
  </p>
  <p>
    In the following we&rsquo;ll try to look into some of the things one can do with dynamic emote parsing. This is general enough so that we won&rsquo;t give code examples; how to implement these necessarily depends on the system.
  </p>
  <h4>Recognition system</h4>
  <blockquote>
   <span class="command">&gt; /self stands by the bar.</span><br/><br/>
   <span class="boldd">You see:</span>&nbsp;<span class="italicd">Griatch stands by the bar.</span><br/>
   <span class="boldd">Others see:</span>&nbsp;<span class="italicd">A tall man stands by the bar.</span>
  </blockquote>
  <p>
    A recognition system
    is a common system to embed into roleplaying MUDs, regardless of actor or director stance (although it technically belongs on the actor-stance side). A recognition system permeates the entire game, not only the emoting. The basic premise is that each character is only recognized by their description (or a shortened version of it) until they have been &ldquo;recognized&rdquo;. Recognition could be automatic (setting the real character name when you first talk to them) or arbitrary (assign whatever name you like to them). Some games also include features like cloaks and masks to temporarily make identification harder or impossible.
  </p>
  <h4>Language parsing</h4>
  <blockquote>
   <span class="command">&gt; emote Griatch says &ldquo;[elvish]This is a good day.&rdquo;</span><br/><br/>
   <span class="boldd">Elvish-speakers see:</span><span class="italicd">&nbsp;Griatch says &ldquo;[elvish]This is a good day.&rdquo;</span><br/>
   <span class="boldd">Others see:</span><span class="italicd">&nbsp;Griatch says &ldquo;Ni&rsquo;ea fe dolae.&rdquo;</span>
  </blockquote>
  <p>
   This essentially deprecates the <span class="italicd">say</span> command, making speaking a coded part of the emote while including (maybe optional) language parsing of everything within the quotes. Different levels of elvish skill level can be checked to determine if all words are understood or if some are scrambled.
  </p>
  <h4>Grammatical cues</h4>
  <p>
    Parsing for grammatical cues is mainly relevant for systems where different people see different things (actor stance). They allow the player to help the parser produce grammatically correct text regardless of who sees it. For English output it generally requires the involved character&rsquo;s gender to be accessible by the parser.
  </p>
  <blockquote>
   <span class="command">&gt; emote ~Griatch smiles, but ^Griatch hand is shaking.</span><br/><br/>
   <span class="boldd">You see:&nbsp;</span><span class="italicd">You smile, but your hand is shaking.</span><br/>
   <span class="boldd">Others see</span>:&nbsp;<span class="italicd">Griatch smiles but his hand is shaking.</span>
  </blockquote>
  <h4>Command parsing</h4>
  <blockquote>
   <span class="command">&gt; emote Shouting, /self #attacks &nbsp;/tom, aiming for his /head.</span><br/>
   <span class="italicd">Shouting,&nbsp;Griatch attacks Tom, aiming for his head.</span>
  </blockquote>
  <p>
    With clever parsing, entire command structures could be embedded into the natural language of the emote. In the example above, the command &ldquo;
   <span class="italicd command">hit tom head</span>
   <span class="italicd">&rdquo;</span>&nbsp;has been embedded into the command through the emote mini-language.
  </p>
  <h4>Limited-scope emote</h4>
  <blockquote>
    <span class="command">&gt; hemote Secretly, Griatch sneaks along the wall.</span><br/>
    <span class="command">&gt; memote In the matrix, Griatch&rsquo;s avatar is glowing.</span>
  </blockquote>
  <p>
    These are subtypes of emotes that affect how the emote is perceived codedly. A &ldquo;hidden emote&rdquo; could be used when the character using it is currently sneaking &mdash;&nbsp;some sort of detection skill check might determine which player would<span class="italicd">actually</span>&nbsp;see it. Other variations might be emotes for actions that cause no sound or which are only visible to those of a particular magical aptitude or who are jacked into the matrix. See the section about Actor and Director stances, above, for a discussion on the rationale of hiding information from players in this way.
  </p>
  <h4>Remote emote</h4>
  <p>
    Games built around storytelling may allow you to &ldquo;throw&rdquo; your emote in order to make it seem to appear to come from an otherwise mute non-AI NPC, or to show an environmental event happening. This is often handled by a separate set of commands, such as &ldquo;emit&rdquo; or &ldquo;spoof&rdquo;. The emote can be parsed as any other emote or be completely freeform. The problem of attribution (which player is responsible for a given emote) is still there though, so having the game log who issues such commands is probably a good idea.
  </p>
  <h4>Freeform emote with quality evaluation</h4>
  <p>
    The very act of emoting can also be made a part of the game play. Some RP-heavy MUDs do this by letting some (or all) XP be gained on the basis of emoting. Depending on just how that XP can be spent, this could have the advantage of making sure any character archetype is viable. It can also potentially remove the concept of grinding certain skills (although some games combine skill-usage XP with emote-based XP).
  </p>
  <p>
    Technically the evaluation is done by somehow assigning some sort of numerical &ldquo;quality&rdquo; to each emote. This quality could be found from a simple emote-length threshold or be determined via a series of measures such as minimal or optimal length, spell-checking, the avoidance of repeats, the number of people emoting back to you and so on. The exact algorithm is debatable. One could argue that judging quality based on spelling may be seen as excluding players with various writing disabilities.
  </p>
  <p>
    Once the emote quality is determined, this could then translate directly into an XP gain per emote. Alternatively one could instead let the XP-gain be time-dependent and award a fixed amount as long as the player makes enough &ldquo;quality&rdquo; emotes in that time frame. One could also picture a system where, when attaching an emote to a command, the &ldquo;quality&rdquo; of the emote could help give a bonus to said command.
  </p>
  <p>
    Needless to say, emote quality evaluation systems are mainly of interest to very roleplay-focused games. The drawback can potentially be emote-spamming if the system can be doctored or is not properly monitored.
  </p>
  <h4>Fully freeform</h4>
  <p>
    At the far end of the scale we finally have the fully freeform emote without any bells and whistles. This is by far the simplest to implement and is something for pure storytelling games or &ldquo;talker&rdquo;- style servers. Even so, some sort of marker (or log) of who is responsible for each emote is probably useful to help keep order.
  </p>
  <h3>Conclusions</h3>
  <p>
    Whereas the choice of emote system may be a no-brainer for some designers, emoting is arguably the core mechanic for supporting any form of more advanced roleplay. When deciding what we want for our own games we tend to instinctively go with what we are most familiar with (i.e. what our favourite or first MUD uses). I hope this article will help give you some more ideas and allow you to at least make the choice consciously.
  </p>
{% endblock %}
{% block article_bio_content %}
	Griatch is the lead developer of the <a class="softwaretitle" href="http://www.evennia.com">Evennia</a></span>&nbsp;MUD development system.
{% endblock %}
